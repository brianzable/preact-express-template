import { styled } from "../../stitches.config";
import { mauve, tomato } from "@radix-ui/colors";
import { CSS } from "@stitches/react/types/css-util";

export const ParagraphStyles: CSS = {
  marginTop: 0,
  marginBottom: 20,
  color: mauve.mauve11,
  fontSize: 15,
  lineHeight: 1.5,
};

export const FormErrorStyles: CSS = {
  color: tomato.tomato9,
  fontSize: 15,
  listStyleType: "none",

  "&:first-letter": {
    textTransform: "capitalize",
  },
};

const Flex = styled("div", { display: "flex" });

const Text = styled("p", ParagraphStyles);

export { Flex, Text };
