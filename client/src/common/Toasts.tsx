import { red, green } from "@radix-ui/colors";
import * as Toast from "@radix-ui/react-toast";
import { keyframes } from "@stitches/react";
import { styled } from "../../stitches.config";

const hide = keyframes({
  "0%": { opacity: 1 },
  "100%": { opacity: 0 },
});

const slideIn = keyframes({
  from: { transform: `translateX(calc(100% + 25px))` },
  to: { transform: "translateX(0)" },
});

const swipeOut = keyframes({
  from: { transform: "translateX(var(--radix-toast-swipe-end-x))" },
  to: { transform: `translateX(calc(100% + 25px))` },
});

const ToastRoot = styled(Toast.Root, {
  borderRadius: 6,
  boxShadow:
    "hsl(206 22% 7% / 35%) 0px 10px 38px -10px, hsl(206 22% 7% / 20%) 0px 10px 20px -15px",
  padding: 15,
  display: "grid",
  gridTemplateAreas: '"title action" "description action"',
  gridTemplateColumns: "auto max-content",
  columnGap: 15,
  alignItems: "center",

  '&[data-state="open"]': {
    animation: `${slideIn} 150ms cubic-bezier(0.16, 1, 0.3, 1)`,
  },
  '&[data-state="closed"]': {
    animation: `${hide} 100ms ease-in`,
  },
  '&[data-swipe="move"]': {
    transform: "translateX(var(--radix-toast-swipe-move-x))",
  },
  '&[data-swipe="cancel"]': {
    transform: "translateX(0)",
    transition: "transform 200ms ease-out",
  },
  '&[data-swipe="end"]': {
    animation: `${swipeOut} 100ms ease-out`,
  },

  variants: {
    color: {
      green: {
        backgroundColor: green.green9,
      },
      red: {
        backgroundColor: red.red9,
      },
    },
  },
});

const ToastTitle = styled(Toast.Title, {
  gridArea: "title",
  marginBottom: 5,
  fontWeight: 500,
  fontSize: 15,
  color: "white",
});

const ToastDescription = styled(Toast.Description, {
  gridArea: "description",
  margin: 0,
  fontSize: 13,
  lineHeight: 1.3,
  color: "white",
});

const CommonToast = ({
  isOpen,
  onOpenChange,
  title,
  description,
  variant,
}: {
  isOpen: boolean;
  onOpenChange: () => void;
  title?: string;
  description: string;
  variant: any;
}) => {
  console.log(variant);
  return (
    <ToastRoot open={isOpen} onOpenChange={onOpenChange} color={variant}>
      {title && <ToastTitle>{title}</ToastTitle>}
      <ToastDescription>{description}</ToastDescription>
    </ToastRoot>
  );
};

export { CommonToast };
