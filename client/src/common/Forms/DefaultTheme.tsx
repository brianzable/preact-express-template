import { RegistryWidgetsType, RJSFValidationError } from "@rjsf/utils";
import validator from "@rjsf/validator-ajv8";
import { RJSFPasswordInput, RJSFTextInput } from "../../common/Inputs";

const widgets: RegistryWidgetsType = {
  TextWidget: RJSFTextInput,
  PasswordWidget: RJSFPasswordInput,
};

const transformErrors = (errors: RJSFValidationError[]) => {
  return errors.map((error) => {
    if (error.name === "required") {
      error.message = "This field is required";
    }

    return error;
  });
};

const DefaultTheme = {
  validator,
  widgets,
  showErrorList: false,
  transformErrors,
};

export default DefaultTheme;
