import { styled } from "../../../stitches.config";
import { violet } from "@radix-ui/colors";
import { CSS } from "@stitches/react/types/css-util";
import { WidgetProps } from "@rjsf/utils";
import { ChangeEvent, useState } from "preact/compat";
import { Flex } from "..";
import { EyeClosedIcon, EyeOpenIcon } from "@radix-ui/react-icons";

const LabelStyling: CSS = {
  fontSize: 13,
  lineHeight: 1,
  marginBottom: 10,
  color: violet.violet12,
  display: "block",
};

const Label = styled("label", LabelStyling);

const RawTextInput = styled("input", {
  all: "unset",
  flex: "1 0 auto",
  borderRadius: 4,
  padding: "0 10px",
  marginBottom: 3,
  fontSize: 15,
  lineHeight: 1,
  color: violet.violet11,
  boxShadow: `0 0 0 1px ${violet.violet7}`,
  height: 35,
  "&:focus": { boxShadow: `0 0 0 2px ${violet.violet8}` },
});

const InputIconContainer = styled("span", {
  position: "absolute",
  right: 8,
  top: 8,
  cursor: "pointer",
});

export const RJSFPasswordInput = (props: WidgetProps) => {
  const [passwordVisible, setPasswordVisible] = useState(false);

  const handleIconClick = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <Flex css={{ position: "relative" }}>
      <RawTextInput
        type={passwordVisible ? "text" : "password"}
        onChange={(event: ChangeEvent<HTMLInputElement>) =>
          props.onChange((event.target as HTMLInputElement).value)
        }
        disabled={props.disabled}
      />
      <InputIconContainer onClick={handleIconClick}>
        {passwordVisible ? <EyeOpenIcon /> : <EyeClosedIcon />}
      </InputIconContainer>
    </Flex>
  );
};

const RJSFTextInput = (props: WidgetProps) => {
  return (
    <RawTextInput
      onChange={(event: ChangeEvent<HTMLInputElement>) =>
        props.onChange((event.target as HTMLInputElement).value)
      }
      disabled={props.disabled}
    />
  );
};

const Fieldset = styled("fieldset", {
  all: "unset",
  marginBottom: 15,
  width: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-start",
});

export { RawTextInput, RJSFTextInput, Label, Fieldset, LabelStyling };
