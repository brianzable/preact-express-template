import { createBrowserRouter, RouterProvider } from "react-router-dom";
import LoginRegistrationPage from "./Auth/LoginRegistrationForm";
import { globalCss } from "@stitches/react";
import LandingPage from "./LandingPage";
import { LabelStyling } from "./common/Inputs";
import { FormErrorStyles, ParagraphStyles } from "./common";
import * as Toast from "@radix-ui/react-toast";
import { styled } from "../stitches.config";
import { Theme } from "@radix-ui/themes";

const globalStyles = globalCss({
  body: { margin: 0 },
  button: { all: "unset" },
  fieldset: { all: "unset" },
  input: { all: "unset" },

  ".control-label": LabelStyling,

  ".rjsf fieldset": { width: "100%" },

  ".form-group .field": {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },

  ".form-group .field:not(:last-child)": {
    marginBottom: "15px",
  },

  ".field-description": ParagraphStyles,

  ".error-detail": { padding: 0, margin: 0 },

  ".text-danger": FormErrorStyles,
});

const ToastViewport = styled(Toast.Viewport, {
  position: "fixed",
  bottom: 0,
  right: 0,
  display: "flex",
  flexDirection: "column",
  padding: 25,
  gap: 10,
  width: 390,
  maxWidth: "100vw",
  margin: 0,
  listStyle: "none",
  zIndex: 2147483647,
  outline: "none",
});

const router = createBrowserRouter([
  {
    path: "/",
    element: <LandingPage />,
  },
  { path: "/login", element: <LoginRegistrationPage /> },
]);

export function App() {
  globalStyles();

  return (
    <Theme appearance="light" accentColor="grass">
      <Toast.Provider swipeDirection="right">
        <RouterProvider router={router}></RouterProvider>;
        <ToastViewport />
      </Toast.Provider>
    </Theme>
  );
}
