import { useState } from "preact/hooks";

interface ToastState {
  variant: string;
  description: string;
  title: string;
  isOpen: boolean;
}

export const useForm = <FormData,>(defaultFormData: FormData | {}) => {
  const [formState, setFormState] = useState<FormData | {}>(defaultFormData);
  const [toastState, setToastState] = useState<ToastState>({
    variant: "",
    description: "",
    isOpen: false,
    title: "",
  });
  const onOpenChange = () =>
    setToastState({ variant: "", description: "", isOpen: false, title: "" });

  return {
    formState,
    setFormState,
    toastState,
    setToastState,
    onOpenChange,
  };
};
