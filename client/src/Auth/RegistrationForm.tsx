import { axiosClient } from "../../axios.config";
import { IChangeEvent, withTheme } from "@rjsf/core";
import { StrictRJSFSchema, UiSchema } from "@rjsf/utils";
import { Flex } from "../common";
import DefaultTheme from "../common/Forms/DefaultTheme";
import { CommonToast } from "../common/Toasts";
import { useForm } from "../hooks/form";
import { Button } from "@radix-ui/themes";

const schema: StrictRJSFSchema = {
  description: "Enter your information below to create an account",
  type: "object",
  required: ["email", "password", "username"],
  properties: {
    email: {
      type: "string",
      title: "Email address",
      format: "email",
    },
    password: {
      type: "string",
      title: "Password",
      minLength: 8,
    },
    username: {
      type: "string",
      title: "Display name",
    },
  },
};

interface FormData {
  email: string;
  password: string;
  username: string;
}

const uiSchema: UiSchema = {
  "ui:order": ["email", "password", "username"],
  email: {
    "ui:widget": "TextWidget",
  },
  password: { "ui:widget": "PasswordWidget" },
  username: {
    "ui:widget": "TextWidget",
  },
};

const Form = withTheme(DefaultTheme);

const RegistrationForm = () => {
  const { formState, setFormState, toastState, setToastState, onOpenChange } =
    useForm<FormData>({});
  const registerUser = (submitEvent: IChangeEvent<FormData>) => {
    setFormState(submitEvent.formData!);

    axiosClient
      .post("/api/signup", submitEvent.formData)
      .then((_) => {
        setToastState({
          variant: "green",
          description: "Account created, you may now login.",
          isOpen: true,
          title: "Success!",
        });
      })
      .catch(() => {
        setToastState({
          variant: "red",
          description: "An account for this email may already exist.",
          isOpen: true,
          title: "Possible issue",
        });
      });
  };

  return (
    <>
      <Form
        schema={schema}
        uiSchema={uiSchema}
        onSubmit={registerUser}
        formData={formState}
      >
        <Flex css={{ marginTop: 20, justifyContent: "flex-end" }}>
          <Button>Register</Button>
        </Flex>
      </Form>

      <CommonToast {...toastState} onOpenChange={onOpenChange} />
    </>
  );
};

export default RegistrationForm;
