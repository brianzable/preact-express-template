import { styled } from "../../stitches.config";
import * as Tabs from "@radix-ui/react-tabs";
import { violet, mauve, blackA } from "@radix-ui/colors";
import { Flex } from "../common";
import LoginForm from "./LoginForm";
import RegistrationForm from "./RegistrationForm";

const TabsRoot = styled(Tabs.Root, {
  display: "flex",
  flexDirection: "column",
  width: 300,
  boxShadow: `0 2px 10px ${blackA.blackA4}`,
});

const TabsList = styled(Tabs.List, {
  flexShrink: 0,
  display: "flex",
  borderBottom: `1px solid ${mauve.mauve6}`,
});

const TabsTrigger = styled(Tabs.Trigger, {
  all: "unset",
  fontFamily: "inherit",
  backgroundColor: "white",
  padding: "0 20px",
  height: 45,
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  fontSize: 15,
  lineHeight: 1,
  color: mauve.mauve11,
  userSelect: "none",
  "&:first-child": { borderTopLeftRadius: 6 },
  "&:last-child": { borderTopRightRadius: 6 },
  "&:hover": { color: violet.violet11 },
  '&[data-state="active"]': {
    color: violet.violet11,
    boxShadow: "inset 0 -1px 0 0 currentColor, 0 1px 0 0 currentColor",
  },
});

const TabsContent = styled(Tabs.Content, {
  flexGrow: 1,
  padding: 20,
  backgroundColor: "white",
  borderBottomLeftRadius: 6,
  borderBottomRightRadius: 6,
  outline: "none",
});

const LoginPage = () => {
  return (
    <Flex
      css={{
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
      }}
    >
      <TabsRoot defaultValue="login">
        <TabsList aria-label="Login or register">
          <TabsTrigger value="login">Login</TabsTrigger>
          <TabsTrigger value="register">Register</TabsTrigger>
        </TabsList>
        <TabsContent value="login">
          <LoginForm />
        </TabsContent>
        <TabsContent value="register">
          <RegistrationForm />
        </TabsContent>
      </TabsRoot>
    </Flex>
  );
};

export default LoginPage;
