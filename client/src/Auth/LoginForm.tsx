import { useNavigate } from "react-router-dom";
import { axiosClient } from "../../axios.config";
import { IChangeEvent, withTheme } from "@rjsf/core";
import { StrictRJSFSchema, UiSchema } from "@rjsf/utils";
import { Flex } from "../common";
import DefaultTheme from "../common/Forms/DefaultTheme";
import { CommonToast } from "../common/Toasts";
import { useForm } from "../hooks/form";
import { Button } from "@radix-ui/themes";

const schema: StrictRJSFSchema = {
  description: "Enter your email and password to login",
  type: "object",
  required: ["email", "password"],
  properties: {
    email: {
      type: "string",
      title: "Email address",
      format: "email",
    },
    password: {
      type: "string",
      title: "Password",
      minLength: 8,
    },
  },
};

interface FormData {
  email: string;
  password: string;
}

const uiSchema: UiSchema = {
  "ui:order": ["email", "password"],
  email: {
    "ui:widget": "TextWidget",
  },
  password: { "ui:widget": "PasswordWidget" },
  username: {
    "ui:widget": "TextWidget",
  },
};

const Form = withTheme(DefaultTheme);

const LoginForm = () => {
  const navigate = useNavigate();
  const { formState, setFormState, toastState, setToastState, onOpenChange } =
    useForm<FormData>({});

  const loginUser = (submitEvent: IChangeEvent<FormData>) => {
    setFormState(submitEvent.formData!);

    axiosClient
      .post("/api/login", submitEvent.formData)
      .then((_) => {
        navigate("/");
      })
      .catch(() => {
        setToastState({
          variant: "red",
          description:
            "Either an account doesn't exist, or your credentials are incorrect",
          isOpen: true,
          title: "Something went wrong",
        });
      });
  };

  return (
    <>
      <Form
        schema={schema}
        uiSchema={uiSchema}
        onSubmit={loginUser}
        formData={formState}
      >
        <Flex css={{ marginTop: 20, justifyContent: "flex-end" }}>
          <Button>Login</Button>
        </Flex>
      </Form>

      <CommonToast {...toastState} onOpenChange={onOpenChange} />
    </>
  );
};

export default LoginForm;
