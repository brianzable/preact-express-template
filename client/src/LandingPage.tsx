import { axiosClient } from "../axios.config";
import { useEffect, useState } from "preact/hooks";
import { useNavigate } from "react-router-dom";
import { Button } from "@radix-ui/themes";

const LandingPage = () => {
  const [response, setResponse] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    axiosClient
      .get("/api/")
      .then((r) => setResponse(r.data))
      .catch((e: any) => setResponse(e));
  }, []);

  const logout = () => {
    axiosClient.post("/api/logout").then(() => {
      navigate("/login");
    });
  };

  return (
    <div>
      <p>You are logged in {JSON.stringify(response)}</p>
      <Button onClick={logout}>Logout</Button>
    </div>
  );
};

export default LandingPage;
