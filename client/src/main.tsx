import { render } from "preact";
import { App } from "./App";

import "@radix-ui/themes/styles.css";

render(<App />, document.getElementById("app") as HTMLElement);
