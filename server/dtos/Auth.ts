export class RegistrationData {
  public email: string;
  public password: string;
  public username: string;

  constructor(data: any = {}) {
    this.email = data.email ?? null;
    this.password = data.password ?? null;
    this.username = data.username ?? null;
  }
}

export class LoginData {
  constructor(data: any = {}) {
    this.email = data.email ?? null;
    this.password = data.password ?? null;
  }

  public email: string;
  public password: string;
}
