import { NextFunction, Request, Response } from "express";

class IndexController {
  public index = (req: Request, res: Response, next: NextFunction): void => {
    const user = req.user;
    try {
      res.status(200).json({ data: { message: `You are ${user?.email}` } });
    } catch (error) {
      next(error);
    }
  };
}

export default IndexController;
