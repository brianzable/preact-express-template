import { NextFunction, Request, Response } from "express";
import { LoginData, RegistrationData } from "../dtos/Auth";
import { User } from "@prisma/client";
import AuthService from "../services/AuthService";
import UserSerializer from "../serializers/UserSerializer";

class AuthController {
  public authService = new AuthService();

  public signUp = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const registrationData = new RegistrationData(req.body);
      const createdUser: User = await this.authService.signup(registrationData);
      const serializer = new UserSerializer(createdUser);

      res.status(201).json({ data: serializer.serialize() });
    } catch (error) {
      next(error);
    }
  };

  public logIn = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const userData: LoginData = req.body;
      const { cookie, user } = await this.authService.login(userData);
      const serializer = new UserSerializer(user);

      res.setHeader("Set-Cookie", [cookie]);
      res.status(200).json({ data: serializer.serialize() });
    } catch (error) {
      next(error);
    }
  };

  public logOut = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const userData: User | null = req?.user;
      await this.authService.logout(userData);

      res.setHeader("Set-Cookie", ["Authorization=; Max-age=0"]);
      res.status(200).json({ data: {} });
    } catch (error) {
      next(error);
    }
  };
}

export default AuthController;
