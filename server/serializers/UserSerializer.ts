import { User } from "@prisma/client";

class UserSerializer {
  public user: User;

  constructor(user: User) {
    this.user = user;
  }

  serialize() {
    return {
      id: this.user.id,
      email: this.user.email,
    };
  }
}

export default UserSerializer;
