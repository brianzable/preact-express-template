import { hash, compare } from "bcrypt";
import { sign } from "jsonwebtoken";
import { isEmpty } from "lodash";
import { SECRET_KEY } from "../config";
import { RegistrationData, LoginData } from "../dtos/Auth";
import { HttpException } from "../exceptions/HttpException";
import { TokenContents, TokenData } from "../types/Auth";
import { PrismaClient, User } from "@prisma/client";

const prisma = new PrismaClient();

class AuthService {
  public async signup(userData: RegistrationData): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "userData is empty");

    const foundUser = await prisma.user.findUnique({
      where: {
        email: userData.email,
      },
    });

    if (foundUser)
      throw new HttpException(
        422,
        `This email ${userData.email} already exists`
      );

    const hashedPassword = await hash(userData.password, 10);
    const createUserData = {
      ...userData,
      password: hashedPassword,
    };

    return prisma.user.create({ data: createUserData });
  }

  public async login(
    userData: LoginData
  ): Promise<{ cookie: string; user: User }> {
    if (isEmpty(userData)) throw new HttpException(400, "userData is empty");

    const user = await prisma.user.findUnique({
      where: { email: userData.email },
    });

    if (!user) throw new HttpException(422, "Invalid credentials");

    const isPasswordMatching: boolean = await compare(
      userData.password,
      user.password
    );
    if (!isPasswordMatching)
      throw new HttpException(422, "Invalid credentials");

    const tokenData = this.createToken(user);
    const cookie = this.createCookie(tokenData);

    return { cookie, user };
  }

  public async logout(user: User | null): Promise<User> {
    if (!user) throw new HttpException(401, `This user was not found`);

    return user;
  }

  public createToken(user: User): TokenData {
    const dataStoredInToken: TokenContents = { userId: user.id };
    const secretKey: string = SECRET_KEY;
    const expiresIn: number = 60 * 60;

    return {
      expiresIn,
      token: sign(dataStoredInToken, secretKey, { expiresIn }),
    };
  }

  public createCookie(tokenData: TokenData): string {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`;
  }
}

export default AuthService;
