import { RegistrationData, LoginData } from "../dtos/Auth";
import { PrismaClient, User } from "@prisma/client";
import AuthService from "../services/AuthService";

const prisma = new PrismaClient();

export const createUser = async (
  data: RegistrationData = {
    email: "user@example.com",
    password: "password",
    username: "First Last",
  }
): Promise<User> => {
  const authService = new AuthService();

  return authService.signup(data);
};

export const createSession = async (data: LoginData): Promise<string> => {
  const authService = new AuthService();
  const { cookie } = await authService.login(data);

  return cookie;
};

export const createUserAndSession = async (
  data: RegistrationData = {
    email: "user@example.com",
    password: "password",
    username: "First Last",
  }
): Promise<{ user: User; cookie: string }> => {
  const authService = new AuthService();

  await authService.signup(data);

  return authService.login(data);
};

export const clearUsers = async () => {
  await prisma.user.deleteMany();
};
