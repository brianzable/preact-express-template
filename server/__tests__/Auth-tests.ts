import { PrismaClient } from "@prisma/client";
import request from "supertest";
import { App } from "../app";
import { LoginData, RegistrationData } from "../dtos/Auth";
import { clearUsers, createSession, createUser } from "../prisma/factories";
import AuthRoute from "../routes/AuthRoutes";

const prisma = new PrismaClient();

describe("AuthRoutes", () => {
  let authRoute: AuthRoute;
  let app: App;

  beforeEach(() => {
    authRoute = new AuthRoute();
    app = new App([authRoute]);
  });

  afterEach(clearUsers);

  describe("[POST] /api/signup", () => {
    let registrationData: RegistrationData;

    beforeEach(() => {
      registrationData = {
        email: "user@example.com",
        password: "password",
        username: "First Last",
      };
    });

    it("creates a user", async () => {
      const response = await request(app.getServer())
        .post("/api/signup")
        .send(registrationData);

      expect(response.statusCode).toEqual(201);

      const data = response.body.data;
      expect(data.id).not.toBeFalsy();
      expect(data.email).toEqual("user@example.com");

      const createdUser = await prisma.user.findFirst();
      expect(createdUser?.email).toEqual("user@example.com");
      expect(createdUser?.username).toEqual("First Last");
    });

    it("returns a 401 when an email tries registering twice", async () => {
      await request(app.getServer()).post("/api/signup").send(registrationData);
      const response = await request(app.getServer())
        .post("/api/signup")
        .send(registrationData);

      expect(response.statusCode).toEqual(422);

      const userCount = await prisma.user.count();
      expect(userCount).toEqual(1);
    });

    it("500s if you don't send any data", async () => {
      const response = await request(app.getServer())
        .post("/api/signup")
        .send({});

      expect(response.statusCode).toEqual(500);

      const userCount = await prisma.user.count();
      expect(userCount).toEqual(0);
    });
  });

  describe("[POST] /api/login", () => {
    let loginData: LoginData;

    beforeEach(async () => {
      loginData = {
        email: "user@example.com",
        password: "password",
      };

      await createUser({
        email: loginData.email,
        password: loginData.password,
        username: "First Last",
      });
    });

    it("returns a cookie and user data on successful login", async () => {
      const response = await request(app.getServer())
        .post("/api/login")
        .send(loginData);

      expect(response.headers["set-cookie"][0]).toMatch(
        /^Authorization=.+; HttpOnly; Max-Age=3600;/
      );

      const data = response.body.data;
      expect(data.id).not.toBeFalsy();
      expect(data.email).toEqual("user@example.com");
    });

    it("returns a 401 on invalid login", async () => {
      const response = await request(app.getServer())
        .post("/api/login")
        .send({ ...loginData, password: "invalid" });

      expect(response.headers["set-cookie"]).toBeUndefined();
      expect(response.status).toEqual(422);
    });
  });

  describe("[POST] /logout", () => {
    let existingSessionCookie: string;

    beforeEach(async () => {
      await createUser({
        email: "user@example.com",
        password: "password",
        username: "First Last",
      });

      existingSessionCookie = await createSession({
        email: "user@example.com",
        password: "password",
      });
    });

    it("logout Set-Cookie Authorization=; Max-age=0", async () => {
      const response = await request(app.getServer())
        .post("/api/logout")
        .set("Cookie", existingSessionCookie);

      expect(response.headers["set-cookie"][0]).toEqual(
        "Authorization=; Max-age=0"
      );
    });
  });
});
