import request from "supertest";
import { App } from "../app";
import { clearUsers, createUserAndSession } from "../prisma/factories";
import IndexRoute from "../routes/IndexRoutes";

describe("IndexRoute", () => {
  let indexRoute: IndexRoute;
  let app: App;
  let cookie: string;

  beforeEach(async () => {
    indexRoute = new IndexRoute();
    app = new App([indexRoute]);

    const loginResponse = await createUserAndSession();
    cookie = loginResponse.cookie;
  });

  afterEach(clearUsers);

  describe("[GET] /", () => {
    it("returns a 200 with a message for logged in users", async () => {
      const response = await request(app.getServer())
        .get("/api/")
        .set("Cookie", cookie);

      expect(response.statusCode).toEqual(200);

      const data = response.body.data;
      expect(data.message).toEqual("You are user@example.com");
    });

    it("returns a 401 when the user is not logged in", async () => {
      const response = await request(app.getServer()).get("/api/");

      expect(response.statusCode).toEqual(401);
    });
  });
});
