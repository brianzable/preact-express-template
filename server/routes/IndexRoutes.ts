import { Router } from "express";
import IndexController from "../controllers/IndexController";
import authMiddleware from "../middlewares/auth";
import { Route } from "../types";

class IndexRoute implements Route {
  public path = "/api";
  public router = Router();
  public indexController = new IndexController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authMiddleware, this.indexController.index);
  }
}

export default IndexRoute;
