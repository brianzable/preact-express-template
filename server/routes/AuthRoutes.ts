import { Router } from "express";
import AuthController from "../controllers/AuthController";
import authMiddleware from "../middlewares/auth";
import { Route } from "../types/index";

class AuthRoute implements Route {
  public router = Router();
  public authController = new AuthController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post("/api/signup", this.authController.signUp);
    this.router.post("/api/login", this.authController.logIn);
    this.router.post("/api/logout", authMiddleware, this.authController.logOut);
  }
}

export default AuthRoute;
