import { Router, Request } from "express";
import { User } from "@prisma/client";

export interface Route {
  path?: string;
  router: express.Router;
}

declare global {
  namespace Express {
    export interface Request {
      user: User | null;
    }
  }
}
