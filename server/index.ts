import { App } from "./app";
import IndexRoute from "./routes/IndexRoutes";
import AuthRoutes from "./routes/AuthRoutes";

const app = new App([new AuthRoutes(), new IndexRoute()]);

app.listen();
